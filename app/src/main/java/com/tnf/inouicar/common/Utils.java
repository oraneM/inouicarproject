package com.tnf.inouicar.common;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Calendar;

public abstract class Utils {

    private static int referenceDateJava = 1900;

    public static int realYear (int year) {
        return year - referenceDateJava;
    }

    public static int realMonth (int month) {
        return month + 1;
    }

    public static long getCurrentDate() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTimeInMillis();
    }

    public static String getRegexEmail() {
        return "^[a-zA-Z0-9]+@[a-zA-Z]+.[a-zA-Z]+$";
    }

    public static String getRegexSiret() {
        return "^[0-9]{14}$";
    }

    public static Boolean isEmail(String id) {
        if (id.matches(getRegexEmail())) {
            return true;
        } else {
            return false;
        }
    }

    public static Boolean isSiret(String id) {
        if (id.matches(getRegexSiret())) {
            return true;
        } else {
            return false;
        }
    }

    public static String getOnlyYearMonthAndDayFrom(Timestamp date) {
        return date.toString().split(" ")[0];
    }

}
