package com.tnf.inouicar.controller;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.tnf.inouicar.R;
import com.tnf.inouicar.dao.UserDao;
import com.tnf.inouicar.pojos.User;

public class SignUpActivity extends MenuActivity {

    /* WIDGETS */
    private Button signUpButton;
    private EditText passwordEditText;
    private RadioGroup radioGroupUserStatus;
    private RadioButton radioButtonUserStatus;
    private EditText idEditText;

    /* UTILS */
    private String password;
    private User user;
    private boolean professional;
    private String id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up);

        this.initParamsUserWidgets();
        this.setWidgetsVisibiliy();

        this.signUpButton = findViewById(R.id.signUpButton);
        this.signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getParamsUser();
                if (userIsValid()) {
                    Intent mainActivity = new Intent(SignUpActivity.this, MainActivity.class);
                    mainActivity.putExtra("currentUser", user);
                    startActivity(mainActivity);
                }
            }
        });
    }

    protected void setWidgetsVisibiliy() {
        this.passwordEditText.setVisibility(View.INVISIBLE);
        this.idEditText.setVisibility(View.INVISIBLE);
    }

    public void onRadioButtonClicked(View v) {
        this.radioGroupUserStatus = findViewById(R.id.userStatus);
        int radioButtonClickedId = this.radioGroupUserStatus.getCheckedRadioButtonId();
        this.radioButtonUserStatus = findViewById(radioButtonClickedId);
        this.setWidgetsVisibiliy();
        this.passwordEditText.setVisibility(View.VISIBLE);
        if (radioButtonClickedId == R.id.individual) {
            this.idEditText.setVisibility(View.VISIBLE);
            this.idEditText.setHint("Email");
            this.idEditText.setInputType( InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS );
            this.professional = false;
        }
        if (radioButtonClickedId == R.id.professional) {
            this.idEditText.setVisibility(View.VISIBLE);
            this.idEditText.setHint("SIRET");
            this.professional = true;
        }
    }

    protected void initParamsUserWidgets() {
        this.passwordEditText = findViewById(R.id.passwordEditText);
        this.idEditText = findViewById(R.id.id);
    }

    protected void getParamsUser() {
        this.password = this.passwordEditText.getText().toString();
        this.id = this.idEditText.getText().toString();
        this.user = new User(this.professional, this.id, this.password);
    }

    protected Boolean userIsValid() {
        if (UserDao.isUser(this.id)) {
            if ((UserDao.getUser(this.id).getPassword()).equals(password)) {
                return true;
            } else {
                Toast.makeText(SignUpActivity.this, "Mot de passe eronné !", Toast.LENGTH_SHORT).show();
                return false;
            }
        } else {
            Toast.makeText(SignUpActivity.this, "Utilisateur inconnu !", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

}
