package com.tnf.inouicar.controller;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.tnf.inouicar.R;
import com.tnf.inouicar.dialogs.ValidateDisconnectionDialog;

public class DisconnectionActivity extends MenuActivity {

    /* WIDGETS */
    private Button deconnectionButton;
    private TextView deconnectionText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.disconnection);

        this.deconnectionText = findViewById(R.id.deconnection);
        this.deconnectionText.setText("Vous êtes déconnecté !");

        this.deconnectionButton = findViewById(R.id.backToMainActivity);
        this.deconnectionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent backToMainActivity = new Intent(DisconnectionActivity.this, MainActivity.class);
                startActivity(backToMainActivity);
            }
        });

    }
}
