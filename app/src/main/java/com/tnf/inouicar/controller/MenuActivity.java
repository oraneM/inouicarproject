package com.tnf.inouicar.controller;

import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.tnf.inouicar.R;
import com.tnf.inouicar.dialogs.ValidateDisconnectionDialog;
import com.tnf.inouicar.pojos.User;

public class MenuActivity extends AppCompatActivity {

    protected User user;
    protected Boolean connection;
    protected String idPreviousActivity;

    public MenuActivity() {

    }

    public String getId() {
        String id = this.getClass().getCanonicalName();
        Log.d("ID :", id);
        return id;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        this.tryToGetConnection();
        this.getPreviousActivity();
        this.updateItemsVisibility(menu);
        return true;
    }

    protected void getPreviousActivity() {
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra("activity")) {
                this.idPreviousActivity = intent.getStringExtra("activity");
            } else {
                this.idPreviousActivity = null;
            }
        }
    }

    protected void updateItemsVisibility(Menu menu) {
        MenuItem item;
        if (this.connection) {
            item = menu.findItem(R.id.signUp);
            item.setVisible(false);
            item = menu.findItem(R.id.register);
            item.setVisible(false);
        }
        if (!this.connection) {
            item = menu.findItem(R.id.currentRenting);
            item.setVisible(false);
            item = menu.findItem(R.id.deconnect);
            item.setVisible(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.mainActivity) {
            Intent mainActivity = new Intent(this, MainActivity.class);
            putUser(mainActivity);
            startActivity(mainActivity);
            return true;
        }
        else if (item.getItemId() == R.id.currentRenting) {
            Intent currentRentings = new Intent(this, MyRentingsActivity.class);
            putUser(currentRentings);
            startActivity(currentRentings);
            return true;
        }
        else if (item.getItemId() == R.id.about) {
            Intent about = new Intent(this, AboutActivity.class);
            putUser(about);
            startActivity(about);
            return true;
        }
        else if (item.getItemId() == R.id.signUp) {
            Intent signUp = new Intent(this, SignUpActivity.class);
            putUser(signUp);
            startActivity(signUp);
            return true;
        }
        else if (item.getItemId() == R.id.register) {
            Intent register = new Intent(this, UserRegisterActivity.class);
            putUser(register);
            startActivity(register);
            return true;
        }
        else if (item.getItemId() == R.id.deconnect) {
            this.openDisconnectionDialog();
            // L'absence du putUser(intent) n'est pas derangeante car c'est une deconnexion
            // Donc l'activite suivante ne recevra jamais de User -> ca marche systematiquement
            // Le putUser(intent) est complique a utilise car l'intent est dans le dialog, pas ici
            // peut etre essayer de creer l'intent ici et de le passer en parametre pour le Dialog
            return true;
        }
        else if (item.getItemId() == R.id.searchAgency) {
            Intent agency = new Intent(this, AgencyListActivity.class);
            putUser(agency);
            startActivity(agency);
            return true;
        }
        else {
            return super.onOptionsItemSelected(item);
        }
    }

    protected void putUser(Intent i) {
        if (connection) {
            i.putExtra("currentUser", this.user);
        }
    }

    protected void openDisconnectionDialog () {
        ValidateDisconnectionDialog dialog = new ValidateDisconnectionDialog(MenuActivity.this);
        dialog.show(getSupportFragmentManager(), "A quoi ça sert ????");
    }

    protected void tryToGetConnection() {
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra("currentUser")) {
                this.user = (User) intent.getSerializableExtra("currentUser");
                if (this.user != null) {
                    this.connection = true;
                } else {
                    this.connection = false;
                }
            } else {
                this.user = null;
                this.connection = false;
            }
        }
    }



}
