package com.tnf.inouicar.controller;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.tnf.inouicar.R;
import com.tnf.inouicar.common.Utils;
import com.tnf.inouicar.dao.VehicleDao;
import com.tnf.inouicar.pojos.Agency;
import com.tnf.inouicar.pojos.Vehicle;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class VehicleListActivity extends MenuActivity {

    private String typeVehicle;
    private int doorsNumber;
    // private Date beginDate;
    // private Date endDate;
    private long beginDate;
    private long endDate;
    private Agency arrivalAgency;
    private Agency departureAgency;
    private List<Vehicle> selectedVehicles;
    private Button backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vehicle_list);

        this.getParamsUserChoice();
        this.displayVehicleList();

        this.selectedVehicles = new ArrayList<Vehicle>();
        this.selectedVehicles = VehicleDao.getSelectedVehicles(this.typeVehicle, this.doorsNumber);

        this.displayVehicleList();

        this.backButton = (Button) findViewById(R.id.backMainActivityButton);
        this.backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent back;
                if (idPreviousActivity.equals("com.tnf.inouicar.controller.MainActivity")) {
                    back = new Intent(VehicleListActivity.this, MainActivity.class);
                } else {
                    back = new Intent(VehicleListActivity.this, AgencyListActivity.class);
                }
                if (connection) {
                    putUser(back);
                }
                startActivity(back);
            }
        });
    }

    public void displayVehicleList() {
        this.debug();
    }

    public void getParamsUserChoice() {
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra("vehicleType")) {
                this.typeVehicle = intent.getStringExtra("vehicleType");
            }
            if (intent.hasExtra("doorsNumber")) {
                this.doorsNumber = intent.getIntExtra("doorsNumber", 0);
            }
            if (intent.hasExtra("beginDate")) {
                this.beginDate = intent.getLongExtra("beginDate",0);
            }
            if (intent.hasExtra("endDate")) {
                this.endDate = intent.getLongExtra("endDate",0);
            }
            if (intent.hasExtra("departureAgency")) {
                this.departureAgency = (Agency) intent.getSerializableExtra("departureAgency");
            }
            if (intent.hasExtra("arrivalAgency")) {
                this.arrivalAgency = (Agency) intent.getSerializableExtra("arrivalAgency");
            }
        }
    }

    public void debug() {
        TextView test = findViewById(R.id.test);
        test.setText("TEST :\n"
                + this.typeVehicle + "\n"
                + this.doorsNumber + "\ndu "
                + Utils.getOnlyYearMonthAndDayFrom(new Timestamp(this.beginDate)) + "\nau "
                + Utils.getOnlyYearMonthAndDayFrom(new Timestamp(this.endDate)) + "\nde "
                + this.departureAgency.getCity() + " à " + this.arrivalAgency.getCity());
    }

}
