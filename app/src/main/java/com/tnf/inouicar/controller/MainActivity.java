package com.tnf.inouicar.controller;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;

import com.tnf.inouicar.R;
import com.tnf.inouicar.common.Utils;
import com.tnf.inouicar.dao.AgencyDao;
import com.tnf.inouicar.dao.VehicleDao;
import com.tnf.inouicar.pojos.Agency;
import com.tnf.inouicar.pojos.User;

import java.util.Calendar;
import java.util.Date;

public class MainActivity extends MenuActivity {

    /* LISTENERS */
    private DatePickerDialog.OnDateSetListener beginDateSetListener;
    private DatePickerDialog.OnDateSetListener endDateSetListener;

    /* WIDGETS */
    private Spinner vehiclesTypesSpinner;
    private Spinner doorsNumbersSpinner;
    private Spinner departureAgencySpinner;
    private Spinner arrivalAgencySpinner;
    private Button endDateButton;
    private Button beginDateButton;
    private Button search;
    private TextView selectionsWarning;

    /* PARAMETERS */
    private String vehicleType;
    private int doorsNumber;
    private long beginDate;
    private long endDate;
    private Agency departureAgency;
    private Agency arrivalAgency;

    /* UTILS */
    private boolean selectionsAreValids;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // INITIALISATION DES CHAMPS
        this.addTypesVehiculesItems();
        this.addDoorsNbItems();
        this.addAgenciesItems();
        this.initBeginDateButton();
        this.initEndDateButton();

        // VALIDATION DE LA RECHERCHE
        this.search = findViewById(R.id.buttonVehicleSearcher);
        this.search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifySelection();
                if (selectionsAreValids) {
                    search();
                }
            }
        });
    }

    public void search () {
        Intent vehicleSearcher = new Intent(MainActivity.this, VehicleListActivity.class);
        vehicleSearcher.putExtra("vehicleType",vehicleType);
        vehicleSearcher.putExtra("doorsNumber",doorsNumber);
        vehicleSearcher.putExtra("beginDate", beginDate);
        vehicleSearcher.putExtra("endDate", endDate);
        vehicleSearcher.putExtra("departureAgency", this.departureAgency);
        vehicleSearcher.putExtra("arrivalAgency", this.arrivalAgency);
        // je put l'ID de mon activity si jamais l'utilisateur veut revenir en arrière
        vehicleSearcher.putExtra("activity", this.getId());
        // Si un user est connecté, je le fais un putExtra dessus
        if (connection) {
            putUser(vehicleSearcher);
        }
        startActivity(vehicleSearcher);
    }

    public void verifySelection() {
        this.vehiclesTypesSpinner = findViewById(R.id.vehicles_types);
        this.vehicleType = this.vehiclesTypesSpinner.getSelectedItem().toString();
        this.doorsNumbersSpinner = findViewById(R.id.doors_numbers);
        this.doorsNumber = Integer.parseInt(this.doorsNumbersSpinner.getSelectedItem().toString());

        this.departureAgencySpinner = findViewById(R.id.departureAgency);
        this.arrivalAgencySpinner = findViewById(R.id.arrivalAgency);
        this.departureAgency = AgencyDao.getAgency(this.departureAgencySpinner.getSelectedItem().toString());
        this.arrivalAgency = AgencyDao.getAgency(this.arrivalAgencySpinner.getSelectedItem().toString());

        String warningMessage="";
        this.selectionsWarning = (TextView) findViewById(R.id.selectionsWarning);
        this.selectionsAreValids = true;

        // TEST SUR LES DATES
        long currentTime = Utils.getCurrentDate();
        if ((this.endDate < currentTime && this.endDate != -1) || (this.beginDate < currentTime  && this.beginDate != -1)) {
            this.selectionsAreValids = false;
            warningMessage = warningMessage.concat("\nSélectionnez des dates futures !");
        }
        if (this.beginDate > this.endDate && this.beginDate != -1 && this.endDate != -1) {
            this.selectionsAreValids = false;
            warningMessage = warningMessage.concat("\nLa date de début de location doit être antérieure à la date de fin !");
        }
        if (this.endDate == -1 || this.beginDate == -1) {
            this.selectionsAreValids = false;
            warningMessage = warningMessage.concat("\nSélectionnez des dates !");
        }
        this.selectionsWarning.setTextColor(Color.RED);
        this.selectionsWarning.setText(warningMessage);
    }

    protected void addAgenciesItems() {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, AgencyDao.getAgenciesCombo());
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.departureAgencySpinner = (Spinner) findViewById(R.id.departureAgency);
        this.departureAgencySpinner.setAdapter(dataAdapter);
        this.arrivalAgencySpinner = (Spinner) findViewById(R.id.arrivalAgency);
        this.arrivalAgencySpinner.setAdapter(dataAdapter);
    }

    public void addTypesVehiculesItems() {
        this.vehiclesTypesSpinner = (Spinner) findViewById(R.id.vehicles_types);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, VehicleDao.getVehiclesTypeCombo());
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.vehiclesTypesSpinner.setAdapter(dataAdapter);
    }

    public void addDoorsNbItems() {
        this.doorsNumbersSpinner = (Spinner) findViewById(R.id.doors_numbers);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, VehicleDao.getDoorsNumberCombo());
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.doorsNumbersSpinner.setAdapter(dataAdapter);
    }

    public void initBeginDateButton() {
        this.beginDateButton = (Button) findViewById(R.id.beginDateButton);
        this.beginDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog beginDatePickerDialog = new DatePickerDialog(MainActivity.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth, beginDateSetListener, year, month, day);
                beginDatePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                beginDatePickerDialog.show();
            }
        });
        beginDate = -1;
        this.beginDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int beginDateYear, int beginDateMonth, int beginDateDayOfMonth) {
                beginDateButton.setText(beginDateDayOfMonth + "/" + Utils.realMonth(beginDateMonth) + "/" + beginDateYear);
                beginDate = (new Date(Utils.realYear(beginDateYear),beginDateMonth,beginDateDayOfMonth)).getTime();
            }
        };
    }

    public void initEndDateButton() {
        this.endDateButton = (Button) findViewById(R.id.endDateButton);
        this.endDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH);
                int day = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog endDatePickerDialog = new DatePickerDialog(MainActivity.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth, endDateSetListener, year, month, day);
                endDatePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                endDatePickerDialog.show();
            }
        });
        endDate = -1;
        this.endDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int endDateYear, int endDateMonth, int endDateDayOfMonth) {
                endDateButton.setText(endDateDayOfMonth + "/" + Utils.realMonth(endDateMonth) + "/" + endDateYear);
                //Date n = new Date();
                //DateFormat longDateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG);
                //SimpleDateFormat formater = new SimpleDateFormat("dd-MM-yy");
                //endDate = Long.parseLong(longDateFormat.format(n));
                endDate = (new Date(Utils.realYear(endDateYear), endDateMonth, endDateDayOfMonth)).getTime();
                //endDate = new Timestamp(endDateYear, endDateMonth + 1, endDateDayOfMonth,0,0,0,0);
            }
        };
    }


}