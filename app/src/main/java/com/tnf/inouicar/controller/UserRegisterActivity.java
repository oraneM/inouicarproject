package com.tnf.inouicar.controller;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.tnf.inouicar.R;
import com.tnf.inouicar.common.Utils;
import com.tnf.inouicar.dao.UserDao;
import com.tnf.inouicar.pojos.User;

import java.util.List;

public class UserRegisterActivity extends MenuActivity {

    /* WIDGETS */
    private EditText passwordEditText;
    private EditText passwordConfirmationEditText;
    private Button registrationButton;
    private EditText idEditText;
    private RadioGroup radioGroupUserStatus;
    private RadioButton radioButtonUserStatus;

    /* PARAMETERS */
    //private String email;
    private String password;
    private String confirmationPassword;
    private User newUser;
    private boolean professional;
    private String id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_register);

        this.initParamsUserWidgets();
        this.setWidgetsVisibiliy();

        this.registrationButton = findViewById(R.id.registerButton);
        this.registrationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getParams();
                if(paramsAreValids()) {
                    UserDao.createUser(newUser);
                    // Log.d("TEST",UserDao.printUserList());
                    Intent validRegistration = new Intent(UserRegisterActivity.this, MainActivity.class);
                    validRegistration.putExtra("currentUser", newUser);
                    startActivity(validRegistration);
                }
            }
        });
    }

    protected void initParamsUserWidgets() {
        this.idEditText = findViewById(R.id.id);
        this.passwordEditText = findViewById(R.id.passwordRegister);
        this.passwordConfirmationEditText = findViewById(R.id.passwordConfirmationRegister);
    }

    protected void setWidgetsVisibiliy () {
        this.passwordEditText.setVisibility(View.INVISIBLE);
        this.idEditText.setVisibility(View.INVISIBLE);
        this.passwordConfirmationEditText.setVisibility(View.INVISIBLE);
    }

    public void onRadioButtonClicked(View v) {
        this.radioGroupUserStatus = findViewById(R.id.userStatus);
        int radioButtonClickedId = this.radioGroupUserStatus.getCheckedRadioButtonId();
        this.radioButtonUserStatus = findViewById(radioButtonClickedId);
        this.setWidgetsVisibiliy();
        this.passwordEditText.setVisibility(View.VISIBLE);
        this.passwordConfirmationEditText.setVisibility(View.VISIBLE);
        if (radioButtonClickedId == R.id.individual) {
            this.idEditText.setVisibility(View.VISIBLE);
            this.idEditText.setHint("Rentrez un email");
            this.idEditText.setInputType( InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS );
            this.professional = false;
        }
        if (radioButtonClickedId == R.id.professional) {
            this.idEditText.setVisibility(View.VISIBLE);
            this.idEditText.setHint("Rentrez un SIRET");
            this.professional = true;
        }
    }

    protected void getParams() {
        this.password = this.passwordEditText.getText().toString();
        this.confirmationPassword = this.passwordConfirmationEditText.getText().toString();
        this.id = this.idEditText.getText().toString();
    }

    protected Boolean paramsAreValids() {
        List<User> users = UserDao.getAllUsers();
        for (User user : users) {
            if (user.isProfessional()) {
                if (user.getSiret().equals(this.id)) {
                    Toast.makeText(UserRegisterActivity.this, "Cet utilisateur existe déjà !", Toast.LENGTH_SHORT).show();
                    return false;
                }
            } else {
                if (user.getEmail().equals(this.id)) {
                    Toast.makeText(UserRegisterActivity.this, "Cet utilisateur existe déjà !", Toast.LENGTH_SHORT).show();
                    return false;
                }
            }
        }
        if (!this.password.equals(this.confirmationPassword)) {
            Toast.makeText(UserRegisterActivity.this, "Les deux mots de passe doivent être identiques !", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (this.password.isEmpty()) {
            Toast.makeText(UserRegisterActivity.this, "Rentrez un mot de passe !", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (this.password.length() <= 3) {
            Toast.makeText(UserRegisterActivity.this, "Les mot de passe doit faire plus de 3 caractères !", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (this.professional) {
            if (this.id.isEmpty()) {
                Toast.makeText(UserRegisterActivity.this, "Rentrez un SIRET !", Toast.LENGTH_SHORT).show();
                return false;
            }
            if (!this.id.matches(Utils.getRegexSiret())) {
                Toast.makeText(UserRegisterActivity.this, "Rentrez un SIRET valide à 14 chiffres !", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        if (!this.professional) {
            if (!this.id.matches(Utils.getRegexEmail())) {
                Toast.makeText(UserRegisterActivity.this, "Rentrez un email valide !", Toast.LENGTH_SHORT).show();
                return false;
            }
            if (this.id.isEmpty()) {
                Toast.makeText(UserRegisterActivity.this, "Rentrez un email !", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        this.newUser = new User(this.professional, this.id, this.password);
        return true;
    }
}
