package com.tnf.inouicar.controller;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.tnf.inouicar.R;

public class AboutActivity extends MenuActivity {

    private TextView aboutText;
    private String about;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about);
        this.setAboutText();
        this.putAboutText();
        this.setStyleText();
    }

    protected void setStyleText() {

    }

    protected void setAboutText () {
        this.about = "";
        this.about = this.about.concat("La société InouiCar est une société spécialisée dans la location de véhicules, quel ce soit pour des " +
                "particuliers, des professionnels ou des entreprises.\n\n" +
                "L’histoire d’Enterprise commence par la vision d’un homme. En 1957, Josh Roberts, pilote décoré de " +
                "la Seconde Guerre mondiale, rentre chez lui, avec l’espoir de fonder une entreprise et une famille.\n\n" +
                "S’inspirant des leçons apprises dans la Marine des États-Unis, à savoir notamment la valeur du travail " +
                "acharné, l’esprit d’équipe et simplement l’importance de faire preuve de discernement dans ses " +
                "actions, Jack adopte un concept nouveau, celui de la location d’automobiles, avec une flotte de " +
                "seulement sept voitures.\n\n" +
                "InouiCar est désormais considéré comme un acteur majeur dans le secteur de la location automobile " +
                "par les grands voyageurs et les personnes amenées à prendre beaucoup la route. La société offre à " +
                "ses clients un service complet de locations de tous types de véhicules et utilitaires et sommes " +
                "présents dans les principaux aéroports, gares et centres-villes. InouiCar est présente dans plus de 30 " +
                "pays et dispose de plus de 7 600 agences dans le monde entier.\n\n" +
                "InouiCar emploie environ 74 000 personnes dans le monde pour un chiffre d'affaires annuel de 15 " +
                "400 000 000 $ en 2018. En France, il y a fin 2018 près de deux cents agences ouvertes");
    }

    protected void putAboutText() {
        this.aboutText = findViewById(R.id.aboutText);
        this.aboutText.setText(this.about);
    }
}
