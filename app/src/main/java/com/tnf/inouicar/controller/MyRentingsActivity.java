package com.tnf.inouicar.controller;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.tnf.inouicar.R;
import com.tnf.inouicar.pojos.User;

public class MyRentingsActivity extends MenuActivity {

    private User currentUser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_rentings);

        this.getCurrentUser();
        if (this.currentUser != null) {
            // affiche l'historique des réservations + réservations en cours
        } else {
            // affiche un message comme quoi pb de user
        }
    }

    protected User getCurrentUser() {
        Intent i = new Intent();
        if (i != null) {
            if (i.hasExtra("currentUser")) {
                this.currentUser = (User) i.getSerializableExtra("currentUser");
                return this.currentUser;
            } else {
                return null;
            }
        }
        return null;
    }
}
