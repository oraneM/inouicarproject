package com.tnf.inouicar.controller;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.tnf.inouicar.R;

public class AgencyListActivity extends MenuActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.agency_list);
    }
}
