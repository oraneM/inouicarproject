package com.tnf.inouicar.dao;

import com.tnf.inouicar.common.Utils;
import com.tnf.inouicar.pojos.User;

import java.util.ArrayList;
import java.util.List;

public abstract class UserDao {

    private static List<User> users;

    protected static void initUsers () {
        users = new ArrayList<User>();
        users.add(new User(false,"a@a.fr", "azerty"));
        users.add(new User(true, "12345678999999", "azerty"));
    }

    public static Boolean isUser(String id) {
        Boolean exists = false;
        initUsers();
        if (Utils.isEmail(id)) {
            for (User u : users) {
                if (!(u.getEmail()==null)) {
                    if(u.getEmail().equals(id)) {
                        exists = true;
                    }
                }
            }
        }
        if (Utils.isSiret(id)) {
            for (User u : users) {
                if (!(u.getSiret()==null)) {
                    if(u.getSiret().equals(id)) {
                        exists = true;
                    }
                }
            }
        }
        return exists;
    }

    public static User getUser(String id) {
        initUsers();
        for (User user : users) {
            if (user.isProfessional()) {
                if (user.getSiret().equals(id)) {
                    return user;
                }
            } else {
                if (user.getEmail().equals(id)) {
                    return user;
                }
            }

        }
        return null;
    }

    public static List<User> getAllUsers() {
        initUsers();
        return users;
    }

    public static void createUser(User user) {
        //users.add(user);
    }

    public static String printUserList() {
        String list = "";
        for (User user : users) {
            list = list + list.concat(user.getEmail() + ", " + user.getPassword() + "\n" );
        }
        return list;
    }

}
