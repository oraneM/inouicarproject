package com.tnf.inouicar.dao;

import com.tnf.inouicar.pojos.Vehicle;

import java.util.ArrayList;
import java.util.List;

public abstract class VehicleDao {

    public static List<Vehicle> getSelectedVehicles(String vehicleType, int doorsNumber) {
        return null;
    }

    public static List<String> getVehiclesTypeCombo() {
        List<String> list = new ArrayList<String>();
        list.add("type 1");
        list.add("type 2");
        list.add("type 3");
        return list;
    }

    public static List<String> getDoorsNumberCombo() {
        List<String> list = new ArrayList<String>();
        list.add("2");
        list.add("4");
        return list;
    }

    /*

    ATTRIBUT :

    String CONNEXION_ERROR
    String EMPTY_QUERY


    METHODES :

    getAllVehicles
    getAllRentedVehicles
    getAllNotRentedVehicles


    dataBaseConnexion ???


    */

}
