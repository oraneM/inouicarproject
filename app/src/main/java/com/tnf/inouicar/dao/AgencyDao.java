package com.tnf.inouicar.dao;

import com.tnf.inouicar.pojos.Agency;

import java.util.ArrayList;
import java.util.List;

public class AgencyDao {

    private static List<Agency> agencies;

    protected static void initAgencies () {
        agencies = new ArrayList<Agency>();
        agencies.add(new Agency("Lyon"));
        agencies.add(new Agency("Paris"));
        agencies.add(new Agency("Montpellier"));
    }

    public static List<String> getAgenciesCombo() {
        initAgencies();
        List<String> combo = new ArrayList<String>();
        for (Agency agency : agencies) {
            combo.add(agency.getCity());
        }
        return combo;
    }

    public static Agency getAgency(String city) {
        initAgencies();
        for (Agency agency : agencies) {
            if (agency.getCity().equals(city)) {
                return agency;
            }
        }
        return null;
    }

}
