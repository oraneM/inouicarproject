package com.tnf.inouicar.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.tnf.inouicar.controller.DisconnectionActivity;
import com.tnf.inouicar.controller.MainActivity;
import com.tnf.inouicar.controller.MenuActivity;

public class ValidateDisconnectionDialog extends AppCompatDialogFragment {

    private AppCompatActivity activity;

    public ValidateDisconnectionDialog(AppCompatActivity a) {
        this.activity = a;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Déconnexion");
        builder.setMessage("Vous êtes sur le point de vous déconnecter. Voulez-vous continuer ?");
        builder.setPositiveButton("Oui", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent backToMainActivity = new Intent(activity, DisconnectionActivity.class);
                startActivity(backToMainActivity);
            }
        });
        builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        return builder.create();
    }
}
