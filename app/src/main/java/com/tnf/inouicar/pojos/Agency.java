package com.tnf.inouicar.pojos;

import java.io.Serializable;

public class Agency implements Serializable {

    private String city;

    public Agency(String city) {
        this.city = city;
    }

    public String getCity () {
        return this.city;
    }

    public void setCity (String city) {
        this.city = city;
    }

}
