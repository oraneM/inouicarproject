package com.tnf.inouicar.pojos;

import java.io.Serializable;

public class User implements Serializable {

    private String email;
    private String password;
    private String siret;
    private boolean professional; // contraire : individual

    public User(boolean professional, String id, String password) {
        this.professional = professional;
        this.password = password;
        if (this.professional) {
            this.siret = id;
            this.email = null;
        } else {
            this.siret = null;
            this.email = id;
        }
    }

    public String getEmail() {
        return this.email;
    }

    public String getSiret() {
        return this.siret;
    }

    public String getPassword() {
        return this.password;
    }

    public Boolean isProfessional() {
        return this.professional;
    }

}
